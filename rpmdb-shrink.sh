#!/bin/sh
# Shrink rpmdb when using BerkeleyDB
# requires db5.2-utils package being installed
set -e

for name in \
	Arch \
	Basenames \
	Conflictname \
	Dirnames \
	Filedigests Filepaths \
	Group \
	Installtid \
	Name Nvra \
	Obsoletename Os \
	Packagecolor Packages Providename Pubkeys \
	Release Requirename \
	Seqno Sha1header Sigmd5 \
	Triggername Version \
	; do
	db=/var/lib/rpm/$name
	test -f "$db" || continue
	db5.2_load -r lsn "$db"
done

rm -rf /var/lib/rpm/{__db.*,log/*}
