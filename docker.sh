#!/bin/sh
set -e

dir=$(dirname "$0")
cd "$dir"

dist=${2:-th}
arch=${3:-x86_64}
root=${1:-~/root-$dist-$arch}

tag=$(echo "$dist" | tr / -)-$arch

# use ftp1 mirror
PLD_MIRROR=http://ftp1.pld-linux.org/dists/$dist

# setup common arguments for poldek
set -- -r $root --up --noask \
	-O 'rpmdef=_install_langs C' \
	-O 'rpmdef=_excludedocs 1' \
	-O 'particle install = no' \
	-O 'keep downloads=yes' \
	--noignore --uniq

case "$dist" in
th|th/*)
	set -- \
		-s "$PLD_MIRROR/PLD/$arch/RPMS/" \
		-s "$PLD_MIRROR/PLD/noarch/RPMS/" \
		"$@"
	;;
ac)
	if [ "$arch" = "x86_64" ]; then
		arch=amd64
	fi
	set -- \
		-O 'auto directory dependencies = no' \
		-s "$PLD_MIRROR/PLD/$arch/PLD/RPMS/" \
		-s "$PLD_MIRROR/updates/$arch/" \
		db4.5-utils \
		"$@"
	;;
esac

# build
rpm -r $root --initdb

set +e
install -d $root/dev/pts
mknod $root/dev/random c 1 8 -m 644
mknod $root/dev/urandom c 1 9 -m 644
mknod $root/dev/full c 1 7 -m 666
mknod $root/dev/null c 1 3 -m 666
mknod $root/dev/zero c 1 5 -m 666
mknod $root/dev/console c 5 1 -m 660
set -e

poldek "$@" -u --caplookup /bin/sh
poldek "$@" -u \
	busybox \
	poldek \
	vserver-packages

# remove unwanted big packages
remove_packages="ca-certificates coreutils"
for pkg in $remove_packages; do
	rpm -r $root -q $pkg && rpm -r $root -e $pkg --nodeps
done

# install busybox links, but remove programs already existing in the system
chroot $root /bin/busybox --install /usr/local/bin
chroot $root sh -c 'for a in /usr/local/bin/*; do p=${a##*/}; if PATH=/usr/sbin:/usr/bin:/sbin:/bin which $p; then rm -v $a; else mv $a /bin; fi; done'

# rpmdb hack for ac
if [ "$dist" = "ac" ]; then
	db5.2_load -r lsn $root/var/lib/rpm/Packages
	db5.2_dump $root/var/lib/rpm/Packages > $root/var/lib/rpm/.Packages.dump
	chroot $root sh -xec "
	test -x /usr/bin/db_load
	# fix db version
	mv /var/lib/rpm/{,.}Packages
	test -f /var/lib/rpm/DB_CONFIG && mv /var/lib/rpm/{,.}DB_CONFIG
	rm -rf /var/lib/rpm/{log,tmp}
	rm -f /var/lib/rpm/*
	test -f /var/lib/rpm/.DB_CONFIG && mv /var/lib/rpm/{.,}DB_CONFIG
	db_load /var/lib/rpm/Packages < /var/lib/rpm/.Packages.dump
	rm -f /var/lib/rpm/.Packages*
	rpm --rebuilddb

	# this ensures that db is definately present
	rpm -qa >/dev/null
	"
fi

# fix netsharedpath, so containers would be able to install when some paths are mounted
sed -i -e 's;^#%_netsharedpath.*;%_netsharedpath /dev/shm:/sys:/proc:/dev:/etc/hostname:/usr/share/doc;' $root/etc/rpm/macros
# disable repackage
sed -i -e '/^#%_repackage_all_erasures/ s/^#//' $root/etc/rpm/macros
# disable docs
sed -i -e '/^#%_excludedocs/ s/^#//' $root/etc/rpm/macros

# use http:// urls, ftp:// urls broken for poldek:
# http://lists.pld-linux.org/mailman/pipermail/pld-devel-en/2019-May/025771.html
sed -i -e 's;ftp://;http://;' $root/etc/poldek/repos.d/*.conf

# remove /root/tmp, it causes TMPDIR initialized to /root/tmp
rm -rf $root/root/tmp

# no need for alternatives
if [ "$dist" != "ac" ]; then
	poldek-config -c $root/etc/poldek/poldek.conf ignore systemd-init
fi

# this makes initscripts to believe network is up
touch $root/var/lock/subsys/network

# cleanup the filesystem from un-needed files
./cleanroot.sh $root

# for nfs-utils and friends
echo /etc/rc.d/rc3.d > $root/var/run/runlevel.dir

# record build date
echo "Image build date: $(date '+%Y-%m-%d %H:%M:%S%z')" > $root/etc/motd
