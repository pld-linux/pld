#!/bin/sh

set -e

snap=${1:-2019}
arch=${2:-x86_64}
dist=${3:-th}

DOCKER_BUILDKIT=1 \
docker build \
	--build-arg=dist=$dist/$snap \
	--build-arg=arch=$arch \
	. \
	-t registry.gitlab.com/pld-linux/pld/snapshot:$snap-$arch
