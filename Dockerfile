# syntax = docker/dockerfile:experimental
#
# Requires Docker v18.06 or later and BuildKit mode to use cache mount
# Docker v18.06 also requires the daemon to be running in experimental mode.
#
# $ DOCKER_BUILDKIT=1 docker build .
#
# See https://github.com/moby/buildkit/blob/master/frontend/dockerfile/docs/experimental.md

ARG BOOTSTRAP_IMAGE=registry.gitlab.com/pld-linux/pld/bootstrap:20210815
ARG dist=th
ARG arch=x86_64

FROM $BOOTSTRAP_IMAGE AS build
ARG dist=th
ARG arch=x86_64

WORKDIR /build
COPY . .

COPY rpmdb-shrink.sh /pld/usr/lib/rpm/rpmdb-shrink
RUN --mount=type=cache,id=poldek,target=/var/cache/poldek \
	sh -x ./docker.sh /pld "$dist" "$arch"

FROM scratch AS runtime
COPY --from=build /pld .

FROM runtime AS util-linux
RUN --mount=type=cache,id=poldek,target=/var/cache/poldek \
	poldek -u util-linux

# Finalize x86_64 target
FROM runtime AS build-x86_64

# Finalize i686 target
FROM runtime AS build-i686
ENTRYPOINT ["setarch", "i686"]
COPY --from=util-linux /usr/bin/setarch /usr/bin/

# Assemble release image
FROM build-$arch AS release
