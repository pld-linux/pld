#!/bin/sh
# script to push new Git tag to remote repo,
# so that tagged build is created,
# which can be distributed to dockerhub
set -e

version=$(date +%Y%m%d)

echo "Press ENTER to create $version Git tag and Docker build"
read a

git tag -am "build $version" $version
git push origin $version

echo 'Press ENTER when build has finished'
read a

# create local "pld" image
# NOTE: to pull registry.gitlab.com images, you need to login
docker pull registry.gitlab.com/pld-linux/pld:$version
docker tag registry.gitlab.com/pld-linux/pld:$version pld

# push to various places
docker tag pld glen/pld:$version
docker push glen/pld:$version

docker tag pld glen/pld:latest
docker push glen/pld:latest
